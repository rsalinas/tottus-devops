FROM node:10-alpine

WORKDIR /usr/src/app

USER node
COPY --chown=node:node . .

EXPOSE 3000
CMD [ "node", "index.js" ]